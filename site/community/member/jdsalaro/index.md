
```{post} 2023-07-18
:author: "@jdsalaro"
:tags: fundador, moderador
:category: miembros
:language: Español, English, Deutsch
:location: Colombia
:excerpt: 1
```

# @jdsalaro

```{figure} index.md-data/cloud-atlas.jpg
---
alt: cloud-atlas
---
Neo Seoul, Year 2144 - [Cloud Atlas](https://en.wikipedia.org/wiki/Cloud_Atlas_(film))
```

Hola soy `@jdsalaro`, conocido como Jayson por todos, junto con los moderadores me metí en ésta aventura! Me gustan los computadores, la política y la filosofía, la cocina asiática, la música de todo tipo, los deportes y los idiomas. 

Mi artista favorito es [Avicii](https://en.wikipedia.org/wiki/Avicii).

Éste es mi espacio en GuayaHack!

## [https://jdsalaro.com](https://jdsalaro.com)

## Where to Find Me

::::{grid} 4 4 8 8
:class-container: landing-grid
:gutter: 1

:::{grid-item-card}
:text-align: center
:link: https://mastodon.social/@jdsalaro
<i class="fa-brands fa-mastodon" style="font-size:2em"></i>
:::

:::{grid-item-card}
:text-align: center
:link: https://twitter.com/jdsalaro
<i class="fa-brands fa-twitter" style="font-size:2em"></i>
:::

:::{grid-item-card}
:text-align: center
:link: https://linkedin.com/in/jdsalaro
<i class="fa-brands fa-linkedin" style="font-size:2em"></i>
:::

:::{grid-item-card}
:text-align: center
:link: https://jdsalaro.com
<i class="fa-solid fa-globe" style="font-size:2em"></i>
:::


:::{grid-item-card}
:text-align: center
:link: https://gitlab.com/jdsalaro
<i class="fa-brands fa-gitlab" style="font-size:2em"></i>
:::

:::{grid-item-card}
:text-align: center
:link: https://github.com/jdsalaro
<i class="fa-brands fa-github" style="font-size:2em"></i>
:::

:::{grid-item-card}
:text-align: center
:link: https://unix.stackexchange.com/users/151026/jdsalaro
<i class="fa-brands fa-stack-exchange" style="font-size:2em"></i>
:::

:::{grid-item-card}
:text-align: center
:link: https://instagram.com/jdsalaro
<i class="fa-brands fa-instagram" style="font-size:2em"></i>
:::

::::

