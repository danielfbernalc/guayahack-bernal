```{post} 2023-06-30
:author: "GuayaHack"
:tags: noticia, organización, incompleto
:category: wiki
:language: Español
:excerpt: 1
```

# Inauguración GuayaHack

Se eligió una fecha tentativa para la inauguración y primera orientación de GuayaHack:

## ¿Cúando?

Tendrá lugar a las `2023‐07‐22T15:30:00Z` UTC y `2023‐07‐22T10:30:00−05:00`) en Colombia 

El objetivo es inaugurar el espacio, presentarnos y explicar la razón de ser de GuayaHack y la forma de trabajo.

Éste evento ha sido agregado al calendario 👉 {doc}`/calendario`

## ¿Dónde?

Por Zoom 👉 <a target="_blank" href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=N3U2bTNjZHYxbzMwNW50aTJrdGk0OTBwbTggZ3VheWFoYWNrQG0&amp;tmsrc=guayahack%40gmail.com"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_es.gif"></a>

## Presentes 

{doc}`@jdsalaro </community/member/jdsalaro/index>`, {doc}`@rioschala </community/member/rioschala/index>`

