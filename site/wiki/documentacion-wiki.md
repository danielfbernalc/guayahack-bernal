```{post} 2023-06-30
:author: "GuayaHack"
:tags: newbie, organización, incompleto
:category: wiki
:language: Español, English, German
:excerpt: 1
```

# La WIKI

Éste artículo contiene las pautas generales con base en las cuales mantenemos nuestra [WIKI](https://guayahack.co/posts/category/wiki/).

## Estructura

### Nombramiento de Archivos

En general, tenemos artículos de varios tipos: `tutorial`, `organizacion`, `evento`, `documentacion`, `infraestructura` y probablemente vendrán más.

La ruta de cada artículo debe comenzar con su tipo a fin de facilitar la edición, por ejemplo `tutorial-intro-uso-vscode.md` y su carpeta de datos correspondiente `tutorial-intro-uso-vscode.md-data`. 



