```{post} 2023-06-30
:author: "GuayaHack"
:tags: organización, incompleto, comunicación
:category: wiki
:language: Español
:excerpt: 1
```

# Comunicación

En GuayaHack tenemos tres vias principales de comunicación con los miembros: Discord, la página y la lista de correo. Cuando exista información de relevancia que debe ser compartida con la comunidad ésta debe ser enviada como mínimo a éstos tres canales.

## main@GuayaHack

Aquí consignaremos las pautas más importantes a considerar durante la comunicación a través de {doc}`/noticias` y demás.

## Discord

Aquí consignaremos las pautas más importantes a considerar durante la comunicación a través de {doc}`/wiki/infraestructura-discord` y demás.

## Newsletter

Aquí consignaremos las pautas más importantes a considerar durante la comunicación a través de {doc}`/wiki/infraestructura-newsletter` y demás.


## Encuestas

A fin de conocer mejor la comunidad, es recomendable realizar encuestas de manera periódica (cada un par de meses).


