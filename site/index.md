
# Main @ GuayaHack

**Bienvenid@s a GuayaHack!** 

Somos un grupo de estudio y [Hackerspace](https://en.wikipedia.org/wiki/Hackerspace) fundado con ayuda de la {doc}`/community/index` por {doc}`/community/member/jdsalaro/index` el cual surgió como idea en [/r/Colombia](https://www.reddit.com/r/Colombia/comments/151fkiz/con_una_prima_y_un_amigo_armaremos_un_grupo_de).

```console
$ date=`date -d @1689530400`; echo -e "\nGuayaHack, fundado: $date"

GuayaHack, fundado: So 16. Jul 20:00:00 CEST 2023
```

## ¿Quién puede Participar?

{doc}`/wiki/organizacion-nivel-novato`, {doc}`/wiki/organizacion-nivel-experimentado` y {doc}`/wiki/organizacion-nivel-profesional` estudiamos, practicamos y compartimos junt@s la programación, la informática y toda tecnología relevante en el mundo moderno entre nosotr@s. Lo único que necesitas hacer para unirte es entrar a nuesro Discord 👇

```{div} discord-widget
<iframe src="https://discord.com/widget?id=1130256195345727560&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
```

## Community

```{toctree}
:maxdepth: 1
:hidden:
:caption: "# GuayaHack ☝️"
# Memorial <community/memorial.md>
# Noticias <noticias.md>
# Calendario <calendario.md>
# Posts <posts/index.md>
# Reglas <community/rules.md>
# Comunidad <community/index.md>
# WIKI <https://guayahack.co/posts/category/wiki/>
# ¿Cómo Ayudo? <https://gitlab.com/guayahack/main/-/issues/>
```
Puedes aprender más sobre GuayaHack y sus miembros en {doc}`/community/index` nuestro {doc}`/community/memorial` o leyendo las [Reglas](community/rules.md).

## Guías, Materiales y Otros

Esa tal documentación no existe, por eso hay que crearla. Escriba primero, piense despues :P

Por ejemplo en la [# WIKI](https://guayahack.co/posts/category/wiki/). 


## Indices and tables

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`
